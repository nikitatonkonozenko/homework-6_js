function createNewUser(firstName, lastName, birthDay) {
    return newUser = {
        firstName,
        lastName,
        birthDay,
        getLogin: function() {
            return this.firstName[0].toLowerCase() + this.lastName.toLowerCase()
        },
        getAge: function() {
            let todayDate = new Date()
            let userAge = new Date(this.birthDay.replace(/(\d{2}).(\d{2}).(\d{4})/, "$3/$2/$1"))
            return Math.floor((todayDate - userAge)/(1000*60*60*24*365))
        },
        getPassword: function() {
            return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthDay.substr(-4)
        },
    }
}

createNewUser(prompt("Введіть ім'я"), prompt("Введіть прізвище"), prompt("Введіть дату народження", "dd.mm.yyyy"))
console.log(newUser.getLogin())
console.log(newUser.getAge())
console.log(newUser.getPassword())

